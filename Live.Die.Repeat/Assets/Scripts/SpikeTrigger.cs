﻿using UnityEngine;
using System.Collections;

public class SpikeTrigger : MonoBehaviour {

	public SpikeTrigger spikePrefab;

	public AudioSource spikeSwitch;
	public AudioClip switchSound;

	public float spikeSpawnTime = 5.0f;
	public float spikeSpawnTimer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (GetComponentInChildren<Rigidbody2D> ().isKinematic == false) {
			spikeSpawnTimer += Time.deltaTime;
			if (spikeSpawnTimer >= spikeSpawnTime) {
				spikeSpawnTimer = 0;
				//Instantiate (spikePrefab, transform.position, transform.rotation);
				//float posX = UnityEngine.Random.Range (minRangeX, maxRangeX);
				//The viewport will go from 0 to 1, in both x and y. (0 is left/bottom, 1 is right/top).
				//Vector3 viewportPosition = new Vector3 (posX, posY, posZ);
				//We convert this to a world position in our scene
				//Vector3 spikePos = Camera.main.ViewportToWorldPoint (viewportPosition);
				//ExitPopup.instance.gameObject.SetActive (true);
				//ExitPopup.instance.myTextfield.text = owner.name + " has won!";

			}
		}
	}

	void OnTriggerEnter2D (Collider2D col)
	{
		if (col.GetComponent<Player> () != null) {
			spikeSwitch.PlayOneShot (switchSound);
			GetComponentInChildren<Rigidbody2D> ().isKinematic = false;
		}
	}
}
