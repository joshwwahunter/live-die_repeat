﻿using UnityEngine;
using System.Collections;

public class BaseUnit : MonoBehaviour
{
	public float movementSpeed;
	public float terrainCheckRaycastDistance = 0.1f;

	public int health;

	public Bullet bulletPrefab;

	public Transform gunBarrel;

	protected float rayCastOffset = 0.5f;

	protected Rigidbody2D rb;
	protected Animator anim;

	public AudioSource playerAudio;

	public AudioClip playerShot;
	public AudioClip playerJump;
	public AudioClip playerFall;
	public AudioClip playerHit;
	public AudioClip playerRecoverAmmo;
	public AudioClip playerSwitch;
	public AudioClip playerPickupHealth;

	void Awake ()
	{
		rb = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
	}

	protected void Move (float horizontal)
	{
		Vector2 vel = rb.velocity;

		vel.x = horizontal * movementSpeed;

		rb.velocity = vel;

		Vector3 scale = transform.localScale;
		//We flip the character sprite based on its speed
		if (horizontal < 0) {
			scale.x = -Mathf.Abs (scale.x);
			anim.SetBool ("IsWalking", true);
		} else if (horizontal > 0) {
			scale.x = Mathf.Abs (scale.x);
			anim.SetBool ("IsWalking", true);
		} else if (horizontal == 0) {
			anim.SetBool ("IsWalking", false);
		}
			

	}

	protected bool CanJump (float offsetX)
	{
		Vector3 origin = transform.position;
		origin.x += offsetX;

		RaycastHit2D hitInfo = Physics2D.Raycast (origin, new Vector2 (0, -1), terrainCheckRaycastDistance);
		Debug.DrawLine (transform.position, hitInfo.point, Color.cyan);

		if (hitInfo.collider != null) {
			anim.SetBool ("IsJumping", false);
			return true;
		} else if (hitInfo.collider == null) {
			anim.SetBool ("IsJumping", true);
			return false;
		}
		return false;
	}

}

/*public class BaseUnit : MonoBehaviour
{
	public float movementSpeed;
	public float terrainCheckRaycastDistance = 0.1f;

	protected float raycastOffset = 0.5f;

	protected Rigidbody2D rb;
	protected Animator anim;

	private MovingPlatform currentPlatform;

	void Awake ()
	{
		rb = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
	}

	protected void Move (float horizontal)
	{
		Vector2 vel = rb.velocity;

		vel.x = horizontal * movementSpeed;

		rb.velocity = vel;

		Vector3 scale = transform.localScale;
		//We flip the character sprite based on its speed
		if (horizontal < 0) {
			scale.x = - Mathf.Abs (scale.x);
		} else if (horizontal > 0) {
			scale.x = Mathf.Abs (scale.x);
		}

		transform.localScale = scale;

		anim.SetFloat ("HorizontalSpeed", Mathf.Abs (vel.x));
	}

	protected bool OnTerrain (float offsetX)
	{
		//The origin of our Raycast is the position, since the pivot is already at its feet
		Vector3 origin = transform.position;
		origin.x += offsetX;

		RaycastHit2D hitInfo = Physics2D.Raycast (origin, new Vector2 (0, -1), terrainCheckRaycastDistance);
		if (hitInfo.collider == null) {
			//If we didn't hit anything, this means that we are no longer on a moving platform.
			if (currentPlatform != null) {
				//We set our parent to be the scene root again (SetParent(null) will do this)
				currentPlatform = null;
				transform.SetParent (null, true);
			}
			return false;
		} else {
			//We only check if we are on a movingplatform, if we weren't on one already
			if (currentPlatform == null) {
				//We are checking if the collider that the Raycast just hit, also has a MovingPlatform component on the same GameObject
				MovingPlatform p = hitInfo.collider.GetComponent<MovingPlatform> ();
				if (p != null) {
					currentPlatform = p;
					transform.SetParent (p.transform, true);
				}
			}
			return true;
		}
	}

}*/