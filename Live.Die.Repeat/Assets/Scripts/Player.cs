﻿using UnityEngine;
using System.Collections;


public class Player : BaseUnit
{
	public float jumpHeight;

	public float deathPosY;

	//private int health = 3;

	public static int ammo = 100;
	public static int score;

	private bool isAlive = true;

	private float shotTimer;
	public float hitTimer;

	public DeadPlayer corpse;
	public Player newPlayer;

	public static bool isDead;
	public static bool isHit;
	public static bool isShooting;
	public static bool isWalking;

	public Transform halo;



	// Use this for initialization
	void Start ()
	{
		health = 3;
		UIManager.hitCounter = 0;
		UIManager.instaDeath = false;
		UIManager.newPlayer = true;
	}

	// Update is called once per frame
	void Update ()
	{
		float horizontalInput = Input.GetAxis ("Horizontal");

		Move (horizontalInput);

		if (CanJump (rayCastOffset) || CanJump (-rayCastOffset)) {
			if (Input.GetKeyDown (KeyCode.Space)) {
				playerAudio.PlayOneShot (playerJump);
				Jump ();
			}
		}

		if (Input.GetMouseButtonDown (0)) {
			if (ammo > 0) {
				isShooting = true;
				playerAudio.PlayOneShot (playerShot);
				Shoot ();
				ammo--;
				//Die ();
			}
		}

	

		if (transform.position.y < deathPosY) {
			Die ();
		}


		if (isShooting) {
			shotTimer += Time.deltaTime;
			anim.SetBool ("IsShooting", true);

		}

		if (isHit) {
			hitTimer += Time.deltaTime;
			playerAudio.PlayOneShot (playerHit);
			anim.SetBool ("IsHit", true);
		}

		if (shotTimer >= 1) {
			isShooting = false;
			anim.SetBool ("IsShooting", false);
			shotTimer = 0;
		}
		if (hitTimer >= 1) {
			isHit = false;
			anim.SetBool ("IsHit", false);
			hitTimer = 0;
		}
		if (health <= 0) {
			anim.SetBool ("IsDead", true);
			Die ();
		}

	}


	void Jump ()
	{
		Vector2 vel = rb.velocity;
		vel.y = jumpHeight;
		rb.velocity = vel;

	}

	void OnCollisionEnter2D (Collision2D coll)
	{
		
	}

	void Die ()
	{
			//GetComponent<BoxCollider2D> ().enabled = false;
			//isAlive = false;

			Instantiate (newPlayer, halo.transform.position, halo.transform.rotation);
		Instantiate (corpse, gunBarrel.transform.position, gunBarrel.transform.rotation);
		Destroy (this.gameObject);
	}

	void ReloadLevel ()
	{
		Application.LoadLevel (0);
	}

	void Shoot ()
	{
		Instantiate (bulletPrefab, gunBarrel.transform.position, gunBarrel.transform.rotation);
	}

	void OnTriggerEnter2D (Collider2D coll)
	{
		if (coll.gameObject.GetComponent<EnemyBullet> () != null){
			isHit = true;
			Destroy (coll.gameObject);
			health --;
			UIManager.hitCounter++;
		}
		if (coll.gameObject.GetComponent<Spike> () != null) {
			playerAudio.PlayOneShot (playerHit);
			isHit = true;
			health -= 3;
			UIManager.instaDeath = true;
		}
	}
		
}