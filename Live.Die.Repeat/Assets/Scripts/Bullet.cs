﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
	public Player owner;

	public float bulletSpeed;


	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{
		transform.Translate (bulletSpeed,0, 0);
	}

	void OnTriggerEnter2D (Collider2D otherColl)
	{
		//This will try to get the player component, if the collider2D has not player component, p will be null
		Player p = otherColl.GetComponent<Player> ();
		if (p != null) {
			//We compare the Player-component that we collided with, to the Player-component that created us
			//If the Player-component is not the same as our owner, it must be an enemy
			if (p != owner) {
				//Destroy (this.gameObject);
				//Destroy (p.gameObject);
				//ExitPopup.instance.gameObject.SetActive (true);
				//ExitPopup.instance.myTextfield.text = owner.name + " has won!";
			}
		}
	}
}





