﻿using UnityEngine;
using System.Collections;

public class Enemy : BaseUnit {

	public EnemyBullet enemyBulletPrefab;

	//public int health = 3;

	public Transform enemyGunBarrel;

	public static bool enemyIsHit = false;

	public static bool naziIsDead;
	public static bool naziIsHit;
	public static bool naziIsShooting;

	public float enemyShotTime = 2.5f;
	public float enemyShotTimer;

	public float enemyHitTimer;

	public bool enemyIsDead = false;


	void Update (){
		if (enemyIsDead == false) {
			Shoot ();

			if (enemyIsHit) {
				enemyHitTimer += Time.deltaTime;
				anim.SetBool ("naziIsHit", true);
			}

			if (enemyHitTimer >= 1) {
				enemyIsHit = false;
				anim.SetBool ("naziIsHit", false);
				enemyHitTimer = 0;
			}

			if (health <= 0) {
				Die ();
			}
		} else {
			anim.SetBool ("naziIsDead", true);
		}
	}

	protected void Shoot (){
		enemyShotTimer += Time.deltaTime;
		if (enemyShotTimer >= enemyShotTime) {
			enemyShotTimer = 0;
			Instantiate (enemyBulletPrefab, enemyGunBarrel.transform.position, enemyGunBarrel.transform.rotation);
		}
	}

	void OnTriggerEnter2D (Collider2D coll)
	{
		if (coll.gameObject.GetComponent<Bullet> () != null){
			if (coll.gameObject.GetComponent<EnemyBullet> () == null) {
			Destroy (coll.gameObject);
			//Debug.Log ("blargle");
			health --;
			enemyIsHit = true;
				}
		}
	}

	void Die ()
	{
		enemyIsDead = true;
		//anim.SetBool ("IsDead", true);
	}
}