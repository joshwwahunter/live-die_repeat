﻿using UnityEngine;
using System.Collections;

public class UIManager : MonoBehaviour {
	public static int hitCounter = 0;
	public static bool instaDeath = false;
	public static bool newPlayer = false;

	public float newTimer = 0;
	protected Animator anim;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}



	// Update is called once per frame
	void Update () {
		if (hitCounter == 1) {
			anim.SetBool ("playerHit1", true);
		} else if (hitCounter == 2) {
			anim.SetBool ("playerHit2", true);
		} else if (hitCounter >= 3) {
			anim.SetBool ("playerHit3", true);
		}

		if (instaDeath == true) {
			anim.SetBool ("InstaDeath1", true);
		}

		if (newPlayer) {
			newTimer += Time.deltaTime;
			anim.SetBool ("newPlayer", true);
			anim.SetBool ("playerHit1", false);
			anim.SetBool ("playerHit2", false);


		}

		if (newTimer >= 1) {
			newPlayer = false;
			newTimer = 0;
				anim.SetBool ("newPlayer", false);
			anim.SetBool ("playerHit3", false);
		}
}
}
